##############################################################################
##############################################################################
# 
# Project : Ricouard, Lehuta, Mahévas (2022) - Are Maximum Yields sustainable ?
# 
# Script : catches
# 
# Obj : Study the catches at equilibrium as a function of parameters
#       
#
# Contact : antoine.ricouard@ifremer.fr
# 
##############################################################################
##############################################################################


Baranov <- function(N, params){
  #the classical Baranov catch equation (1918)
  #return the annual catch given a set of parameters and a population size N
  
  #parameter specification
  Ma <- params$Ma #natural mortality of adults
  Fa <- params$Fa # fishing mortalities 
  Za <- Ma + Fa
  
  #Catch Rate :
  Cr <- ( Fa / Za )*( 1 - exp(-Za) )
  
  Catch <- Cr*N
  return(Catch)
}

### catches in a year ----

  # total catch
TotalCatch_annual <- function(params){
  # calls equilibrium_tot
  # calls Baranov
  a_rec <- params$a_rec
  a_mat <- params$a_mat
  m_rec <- params$m_rec
  m_mat <- params$m_mat
  
  Sh <- params$Sh
  S <- params$S
  
  
  # computing the value of Nsup(m_rep) and Nrec(m_rec) at equilibrium in each case (m_rep <= m_mat and the contrary)
  Nsup_mat <- equilibrium_tot(params)[a_rec+4]
  
  if(params$m_mat <= params$m_rec){
    
    Nrec_mat <- equilibrium_tot(params)[a_rec+2]
    Nsup_rep <- Nrec_mat * (Sh**(12-m_rec)) * (S**(m_rec - m_mat)) +  Nsup_mat * (Sh**(12-m_mat))
    Nrec_rec <- Nrec_mat * (S**(m_rec - m_mat))
    
  }else{
    
    Nrec_mat <- equilibrium_tot(params)[a_rec+3]
    Nsup_rep <- (Nrec_mat  +  Nsup_mat) * (Sh**(12-m_mat))
    Nrec_rec <- Nrec_mat / (Sh**(m_mat-m_rec))
    
  }
  
  
  # compute  the  equilibrium for all month of the year
  Eq_Nsup <- c(Nsup_rep)
  Eq_Nrec <- c(Nrec_rec)
  
  for(m in 1:11){
    Eq_Nsup <- c(Eq_Nsup, Nsup_rep*(Sh**m))
  }
  
  
  for(m in 1:(11-m_rec)){
    Eq_Nrec <- c(Eq_Nrec, Nrec_rec*(Sh**m))
  }
  
  # compute total catch
  C_Nsup <- Baranov(N = Eq_Nsup, params = params)
  C_Nrec <- Baranov(N = Eq_Nrec, params = params)
  TC <- sum(C_Nsup) + sum(C_Nrec)
  
  return(TC)
}

TotalCatch2_annual <- function(Fa,params){
  # idem but depending explicitly on Fa: the function to be optimized when searching the MSY
  #
  # calls params_update02
  # calls TotalCatch_annual
  
  par <- params
  par$Fa <- Fa
  par <- params_update02(par)
  TC <- TotalCatch_annual(par)
  return(TC)
}


### catches in a year as a function of Fa ----

  # total catch
TotalCatchAnnual_Fa <- function(params, Fa_range){
  # Fa is a vector of fishing mortalities
  
  # calls params_update02
  # calls TotalCatch_annual 
  Fa <- Fa_range
  TC <- c()
  for(i in seq(length(Fa_range))){
    par_new <- params
    par_new$Fa <- Fa[i]
    par_new <- params_update02(par_new)
    
    TC <- c(TC, TotalCatch_annual(par_new) )
  }
  
  Out <- data.frame(Fa,TC)
  return(Out)
}


### catches in a year as a function of Fa with varying r, Drec and Dmat ----

  # total catches
TotalCatchAnnual_FaDrec <- function(params, Dmat, Drec_range, Fa_range){
  # in this function Dmat is fixed and Drec varies in Drec_range
  #
  # calls TotalCatchAnnual_Fa
  Drec <- Drec_range
  
  par_new <- params
  par_new$Dmat <- Dmat
  
  Data <- data.frame()
  for(i in seq(length(Drec))){
    
    par_new$Drec <- Drec[i]
    Data_new <- cbind(Drec[i],TotalCatchAnnual_Fa(par_new, Fa_range) )
    Data <- rbind(Data, Data_new)
  }
  
  return(Data)
}

TotalCatchAnnual_FaDmat <- function(params, Drec, Dmat_range, Fa_range){
  # in this function Drec is fixed and Dmat varies in Dmat_range
  #
  # calls TotalCatchAnnual_Fa
  Dmat <- Dmat_range
  
  par_new <- params
  par_new$Drec <- Drec
  
  Data <- data.frame()
  for(i in seq(length(Dmat))){
    
    par_new$Dmat <- Dmat[i]
    Data_new <- cbind(Dmat[i],TotalCatchAnnual_Fa(par_new, Fa_range) )
    Data <- rbind(Data, Data_new)
  }
  
  return(Data)
}

TotalCatchAnnual_FaCstLag <- function(params, Fa_range,  Dmat_range, Lag){
  # Compute the annual total catch with varying Dmat and Drec, 
  # Drec-Dmat is being kept constant and equal to Lag
  #
  # calls TotalCatchAnnual_Fa
  Dmat <- Dmat_range
  Drec <- Dmat + Lag
  
  par_new <- params
  
  Data <- data.frame()
  for(i in seq(length(Dmat))){
    
    par_new$Dmat <- Dmat[i]
    par_new$Drec <- Drec[i]
    Data_new <- cbind(Dmat[i],TotalCatchAnnual_Fa(par_new, Fa_range) )
    Data <- rbind(Data, Data_new)
    
  }
  
  return(Data)
  
}

TotalCatchAnnual_rFa <- function(params, r_range, Fa_range){
  # in this function Dmat and Drec are fixed and given as arguments
  # r varies in r_range
  #
  # calls TotalCatchAnnual_Fa
  r <- r_range
  
  par_new <- params
  Data <- data.frame()
  for(i in seq(length(r))){
    
    par_new$r <- r[i]
    Data_new <- cbind(r[i],TotalCatchAnnual_Fa(par_new, Fa_range) )
    Data <- rbind(Data, Data_new)
  }
  
  return(Data)
}

TotalCatchAnnual0.1_FaDel <- function(ParamSet, Fa_range){
  # revised version of TotalCatchAnnual_FaD... (works for both Dmat and Drec)
  #
  # ParamSet is a set of set of parameters to be explored
  # in this function one Delta is fixed and the other one varies in a certain range
  # 
  # calls TotalCatchAnnual_Fa
  
  if(ParamSet[[1]]$Dmat != ParamSet[[2]]$Dmat){
    
    Data <- data.frame()
    for(i in seq(length(ParamSet))){
      Dmat <- ParamSet[[i]]$Dmat
      Data_new <- cbind(Dmat,TotalCatchAnnual_Fa(ParamSet[[i]], Fa_range) )
      Data <- rbind(Data, Data_new)
    }
    
  }else{
    
    Data <- data.frame()
    for(i in seq(length(ParamSet))){
      
      Drec <- ParamSet[[i]]$Drec
      Data_new <- cbind(Drec,TotalCatchAnnual_Fa(ParamSet[[i]], Fa_range) )
      Data <- rbind(Data, Data_new)
    }
  }
  
  return(Data)
}


### computing numerically the MSY ----
  # in general
TotMSYannual <- function(Fa_range, params){
  # searches numerically the annual MSY
  # calls TotalCatch2_annual
  #
  
  Fa <- Fa_range
  Opt <- optimize( f = TotalCatch2_annual, 
                   interval = c(min(Fa), max(Fa)), 
                   params, 
                   maximum = TRUE )
  MSY <- Opt$objective
  Fa <- Opt$maximum
  Out <- data.frame(Fa,MSY)
  return(Out)
}

  # as a function of one parameter
TotMSYannual_r <- function(params, r_range, Fa_range){
  # searches numerically the annual MSY  for each value of r in a given range
  #
  # calls params_update02
  #       TotMSYannual
  
  r <- r_range
  par_new <- params
  
  Fopt <-c()
  MSY <- c()
  for(i in seq(length(r)) ){
    par_new$r <- r[i]
    par_new <- params_update02(par_new)
    Opt <- TotMSYannual(Fa_range = Fa_range, params = par_new)
    # make sure Fmsy is not the upper bound of interval -> in this case, it's not really a maximum 
    if(max(Fa_range-Opt$Fa > 1e-3)){ # setting the tolerance
      Fopt <- c(Fopt, Opt$Fa)
      MSY <- c(MSY, Opt$MSY)
    }else{
      Fopt <- c(Fopt, NA)
      MSY <- c(MSY, NA)
    }
  }
  
  Out <- data.frame(r, Fopt, MSY)
  return(Out)
} 

TotMSYannual_Dmat <- function(params, Drec, Dmat_range, Fa_range){
  # searches numerically the annual MSY  for each value of Dmat in a given range
  # when Drec is fixed
  #
  # calls params_update02
  #       TotMSYannual
  
  Dmat <- Dmat_range
  par_new <- params
  par_new$Drec <- Drec
  
  Fopt <-c()
  MSY <- c()
  for(i in seq(length(Dmat)) ){
    par_new$Dmat <- Dmat[i]
    par_new <- params_update02(par_new)
    Opt <- TotMSYannual(Fa_range = Fa_range, params = par_new)
    
    # make sure Fmsy is not the upper bound of interval -> in this case, it's not really a maximum 
    if(max(Fa_range)- Opt$Fa > 1e-3 & Opt$Fa-min(Fa_range)> 1e-3){ # setting the tolerance
      Fopt <- c(Fopt, Opt$Fa)
      MSY <- c(MSY, Opt$MSY)
    }else{
      Fopt <- c(Fopt, NA)
      MSY <- c(MSY, NA)
    }
    
  }
  
  Out <- data.frame(Dmat, Fopt, MSY)
  return(Out)
} 

TotMSYannual_Drec <- function(params, Dmat, Drec_range, Fa_range){
  # searches numerically the annual MSY  for each value of Drec in a given range
  # when Drec is fixed
  #
  # calls params_update02
  #       TotMSYannual
  
  Drec <- Drec_range
  par_new <- params
  par_new$Dmat <- Dmat
  
  Fopt <-c()
  MSY <- c()
  for(i in seq(length(Drec)) ){
    par_new$Drec <- Drec[i]
    par_new <- params_update02(par_new)
    Opt <- TotMSYannual(Fa_range = Fa_range, params = par_new)
    
    # make sure Fmsy is not the upper bound of interval -> in this case, it's not really a maximum 
    if(max(Fa_range)-Opt$Fa > 1e-3 & Opt$Fa-min(Fa_range)> 1e-3){ # setting the tolerance
      Fopt <- c(Fopt, Opt$Fa)
      MSY <- c(MSY, Opt$MSY)
    }else{
      Fopt <- c(Fopt, NA)
      MSY <- c(MSY, NA)
    }
  }
  
  Out <- data.frame(Drec, Fopt, MSY)
  return(Out)
} 

TotMSYannual_CstLag <- function(params, Fa_range,  Dmat_range, Lag){
  # searches numerically the annual MSY  for each value of Dmat in a given range
  # Drec-Dmat is being kept constant and equal to Lag
  #
  # calls params_update02
  #       TotMSYannual
  
  Dmat <- Dmat_range
  Drec <- Dmat_range + Lag
  par_new <- params
  
  
  Fopt <-c()
  MSY <- c()
  for(i in seq(length(Dmat)) ){
    par_new$Dmat <- Dmat[i]
    par_new$Drec <- Drec[i]
    par_new <- params_update02(par_new)
    Opt <- TotMSYannual(Fa_range = Fa_range, params = par_new)
    
    # make sure Fmsy is not the upper bound of interval -> in this case, it's not really a maximum 
    if(max(Fa_range)-Opt$Fa > 1e-3 & Opt$Fa-min(Fa_range)> 1e-3){ # setting the tolerance
      Fopt <- c(Fopt, Opt$Fa)
      MSY <- c(MSY, Opt$MSY)
    }else{
      Fopt <- c(Fopt, NA)
      MSY <- c(MSY, NA)
    }
  }
  
  Out <- data.frame(Dmat, Fopt, MSY)
  return(Out)
} 

TotMSYannual0.1_Del <- function(ParamSet, Fa_range){
  # searches numerically the annual MSY  for each value of Dmat/Drexc in a given range
  # when the other one is fixed
  #
  # calls TotMSYannual
  

  Fopt <-c()
  MSY <- c()
  Dmat <- c()
  Drec <- c()
  
  for(i in seq(length(ParamSet)) ){
  
    par <- ParamSet[[i]]
    Dmat <- c(Dmat,par$Dmat)
    Drec <- c(Drec,par$Drec)
    
    Opt <- TotMSYannual(Fa_range = Fa_range, params = par)
    
    # make sure Fmsy is not the upper bound of interval -> in this case, it's not really a maximum 
    if(max(Fa_range)- Opt$Fa > 1e-3 & Opt$Fa-min(Fa_range)> 1e-3){ # setting the tolerance
      Fopt <- c(Fopt, Opt$Fa)
      MSY <- c(MSY, Opt$MSY)
    }else{
      Fopt <- c(Fopt, NA)
      MSY <- c(MSY, NA)
    }
    
  }
  
  if(Dmat[1] != Dmat[2]){
    Out <- data.frame(Dmat, Fopt, MSY)
  }else{
    Out <- data.frame(Drec, Fopt, MSY)
  }
  
  
  return(Out)
} 


  # as a function of two parameters
TotMSYannual_rDmat <- function(params, r_range, Dmat_range, Fa_range){
  # Fmsy for each r when Dmat varies
  #
  # calls TotMSYannual_Dmat
  # calls params_update02
  
  
  r <- r_range
  par_new <- params
  
  Data <- data.frame()
  
  for(i in seq(length(r))){
    par_new$r <- r[i]
    DF <- TotMSYannual_Dmat(params = par_new, Drec = par_new$Drec, Dmat_range, Fa_range)
    Dat_new <- cbind(r[i],  DF)
    Data <- rbind(Data, Dat_new)
  }
  
  return(Data)
}

TotMSYannual_rDrec <- function(params, r_range, Drec_range, Fa_range){
  # Fmsy for each r when Drec varies
  #
  # calls TotMSYannual_Drec
  # calls params_update02
  
  
  r <- r_range
  par_new <- params
  
  Data <- data.frame()
  
  for(i in seq(length(r))){
    par_new$r <- r[i]
    DF <- TotMSYannual_Drec(params = par_new, Dmat = par_new$Dmat, Drec_range, Fa_range)[,1:2]
    Dat_new <- cbind(r[i],  DF)
    Data <- rbind(Data, Dat_new)
  }
  
  return(Data)
}

TotMSYannual_rCstLag <- function(params, r_range, Dmat_range, Fa_range, lag){
  # Fmsy for each r when Dmat varies
  #
  # calls TotMSYannual_CstLag
  # calls params_update02
  
  r <- r_range
  par_new <- params
  
  Data <- data.frame()
  
  for(i in seq(length(r))){
    par_new$r <- r[i]
    DF <- TotMSYannual_CstLag(par_new, Fa_range, Dmat_range, lag)[,1:2]
    Dat_new <- cbind(r[i],  DF)
    Data <- rbind(Data, Dat_new)
  }
  
  return(Data)
}

TotMSYannual0.1_rDel <- function(ParamSet, r_range, Fa_range){
  # revised version of TotMSYannual_rD... (works for both Dmat and Drec)
  # Fmsy for each r when Dmat varies
  #
  # calls TotMSYannual_Dmat
  # calls params_update02
  
  
  r <- r_range
  
  Data <- data.frame()
  Par <- ParamSet
  for(i in seq(length(r))){
    
    for(j in seq(ParamSet)){
      Par[[j]]$r <- r[i] 
    }
    
    DF <- TotMSYannual0.1_Del(ParamSet = Par, Fa_range)
    Dat_new <- cbind(r[i],  DF)
    Data <- rbind(Data, Dat_new)
  }
  
  return(Data)
}


