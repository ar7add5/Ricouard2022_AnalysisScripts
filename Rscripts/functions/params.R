##############################################################################
##############################################################################
# 
# Project : Ricouard, Lehuta, Mahévas (2022) - Are Maximum Yields sustainable ?
# 
# Script : params
# 
# Obj : Parameter specification for the model
# 
# Contact : antoine.ricouard@ifremer.fr
# 
##############################################################################
##############################################################################


### applied sets of parameters ----

  # parameterization from litterature
paramsSole01_litt <- function(){
  # parameters for Bay of Biscay sole, extracted from literature (all except mu and om)
  #
  # NB: when choosing the delay between maturation and recruitment, 
  #     values of Dmat and Drec must be such that m_mat < m_rec 
  #     (in order to perform estimation of mu and om)
  #     these values could modified in the next step, once mu and om are estimated using:
  #      params_update01 or 02
  #
  # acknowledgement : Eloïse Piette-Semeril for the review
  # 
  # references : 
  # Witthames PR et al.(1995). The geographical variation in the potential annual fecundity of dover sole(Solea solea)(L.) from European shelf waters during 1991, Netherlans Journal of Sea Research 34:1-3,45-58
  # WGBIE (2018). Stock Annex: Sole (Solea solea) in divisions 8.a-b (northern and cen-tral Bay of Biscay)
  
  
  params <- list(#raw parameters
    r = 220646, # number of eggs per year (Witthames et al., 1995)
    Ma = 0.1/12, # assumed natural mortality of adults (per month)  - (WGBIE, 2018)
    Fa = 0.1/12, # default fishing mortality of adults (per month) 
    Dmat = 28, # default delay between birth and associated maturation - we must have amat = 2
    Drec =  44 # default delay between birth and associated recruitment - we must have arec = 3
  )

  
  # aggregated parameters - level 1
  
  params$a_mat <- params$Dmat %/% 12 # age of maturation in year
  params$m_mat <- params$Dmat %% 12 # month of maturation
  
  params$a_rec <- params$Drec %/% 12 # age of recruitment in year
  params$m_rec <- params$Drec %% 12 # month of recruitment
  
  params$S <- exp(- params$Ma) #survival rate of unfished mature individuals 
  params$Sh <- exp(- params$Ma - params$Fa) # survival rate of harvested individuals 
  
  
  # aggregated parameters - level 2
  
  b3 <- 0
  for(i in 0:(params$m_mat-1) ){ b3 <- b3 + params$S**(12 - params$m_mat + i) }
  params$beta3 <- b3
  
  b4 <- 0
  for(i in 0:(12-params$m_mat-1) ){ b4 <- b4 + params$S**(i) }
  params$beta4 <- params$beta3 + b4
  
  t3 <- 0
  for(i in 0:(params$m_mat-1) ){ t3 <- t3 + params$Sh**(12 - params$m_rec + i) }
  params$theta3 <- (params$S**(params$m_rec - params$m_mat) ) * t3  
  
  t41 <- 0
  t42 <- 0
  for(i in 0:(params$m_rec - params$m_mat -1) ){ t41 <- t41 + params$S**(i) }
  for(i in 0:(12 - params$m_rec - 1)){ t42 <- t42 + params$Sh**(i) }
  params$theta4 <- params$theta3 + ( t41 + params$S**(params$m_rec - params$m_mat)*t42 )
  
  
  k3 <- 0
  for(i in 0:(params$m_mat-1) ){ k3 <- k3 + params$Sh**(12 - params$m_mat + i) }
  params$kappa3 <- k3
  
  k4 <- 0
  for(i in 0:(12-params$m_mat-1) ){ k4 <- k4 + params$Sh**(i) }
  params$kappa4 <- params$kappa3 + k4
  
  e31 <- 0
  e32 <- 0
  for(i in 0:(params$m_mat - params$m_rec -1)){e31 <- e31 + params$Sh**i}
  for(i in 0:(params$m_rec -1)){e32 <- e32 + params$S**(12-params$m_mat+i) }
  params$eta3 <-  ( (params$S**(12+ params$m_rec -params$m_mat)*e31) + e32 )
  
  e4 <- 0
  for(i in 0:(12-params$m_mat-1) ){e4 <- e4 + params$S**i}
  params$eta4  <- params$eta3 + e4
  

  
  params$psi1 <- params$S**(12-params$m_mat)
  params$psi2 <- (params$S**(params$m_rec-params$m_mat)) * (params$Sh**(12-params$m_rec))
  params$psi3 <- params$Sh**(12-params$m_mat)
  
  params$sigma <- params$S**12
  params$chi <- (params$S**(12+params$m_rec-params$m_mat))*(params$Sh**(params$m_mat-params$m_rec))
  
  
  return(params)
}

  # adding estimated parameters
ParFill <- function(ParLitt, Fit){
  # completes the parameter set with estimed values of om and mu
  # ParLitt is generated by : params...01_litt
  # Fit is the result of non-linear least-square estimation by ParEstim
  #
  # computes aggregated parameters
  
  

  params <- ParLitt
  params$mu <- as.numeric( coef(Fit)['mu'] )
  params$om <- as.numeric( coef(Fit)['om'] )
  
  
  # aggregated parameters - level 2
  
  params$alpha1 <- exp(- params$om * params$m_mat)
  params$alpha2 <- params$alpha1 * exp( -params$om * (12-params$m_mat) )
  
  params$beta1 <- params$mu * params$beta3
  params$beta2 <- params$beta1 + params$mu * (params$beta4 - params$beta3)
    
  params$theta1 <- params$mu * params$theta3 
  params$theta2 <- params$theta1 + params$mu *(params$theta4 - params$theta3)
  
  params$kappa1 <- params$mu * params$kappa3 
  params$kappa2 <- params$kappa1 + params$mu * (params$kappa4 - params$kappa3)
  
  
  # aggregated parameters - level 3
  
  params$sigma <- params$S**12
  params$nu <- params$Sh**12
  params$rho <- (params$S**(params$m_rec-params$m_mat)) * (params$Sh**(12-(params$m_rec-params$m_mat)))
  
  params$alpha <- params$r * params$alpha1 * params$alpha2 ** (params$a_mat)
  params$beta <- params$beta1 + params$a_mat * params$beta2
  params$theta <- params$theta1 + params$a_mat * params$theta2
  params$kappa <- params$kappa1 + params$a_mat * params$kappa2
  
  return(params)
}
ParFill0.1.1 <- function(ParLitt,Fit,Dmat_range){
  # a continuation of ParFill when Fit is given by ParEstim0.1._
  Par <- list()
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  for(i in seq(length(Dmat_range))){
    par_new <- params_DChange(params = par, 
                              Dmat = Dmat_range[i], 
                              Drec = par$Drec)
    Par[[i]] <- ParFill(par_new,Fit[[i]])
  }
  return(Par)
}
ParFill0.1.2 <- function(ParLitt,Fit,Drec_range){
  # a continuation of ParFill when Fit is given by ParEstim0.1._
  Par <- list()
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  for(i in seq(length(Drec_range))){
    par_new <- params_DChange(params = par, 
                              Dmat = par$Dmat, 
                              Drec = Drec_range[i])
    Par[[i]] <- ParFill(par_new,Fit[[i]])
  }
  return(Par)
}
ParFill0.1.3 <- function(ParLitt,Fit,Dmat_range,Lag){
  # a continuation of ParFill when Fit is given by ParEstim0.1._
  Par <- list()
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  for(i in seq(length(Dmat_range))){
    par_new <- params_DChange(params = par, 
                              Dmat =  Dmat_range[i], 
                              Drec = Dmat_range[i]+Lag)
    Par[[i]] <- ParFill(par_new,Fit[[i]])
  }
  return(Par)
}
ParFill0.1.4 <- function(ParLitt,Fit,Dmat_range,Drec_range){
  # a continuation of ParFill when Fit is given by ParEstim0.1._
  Dmat <- Dmat_range
  Drec <- Drec_range
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
 
  Plan <- expand.grid(Dmat,Drec)
  colnames(Plan) <- c('Dmat','Drec')
  Par <- list()
  for(i in seq(nrow(Plan))){
    par_new <- params_DChange(params = par, 
                              Dmat =  Plan$Dmat[i], 
                              Drec = Plan$Drec[i])
    Par[[i]] <- ParFill(par_new,Fit[[i]])
  }
  return(Par)
}


### estimation of missing parameters ----

  # reading any dataset from a file
DataRead_txt <- function(txt_file){
  #read data from a text file
  # use '...' to specify the file to be read
  path <- paste0('data/', txt_file)
  data <- read.table(path,sep = ' ',header = TRUE, row.names = 1, skip = 2)
  return(data)
}

  # transforming data with respect to the species considered in order to make them suitable for the following analyses
DataTrans1_sole <- function(RawData){
  # data transformation for sole
  # very few transformation necessary : just ajusting scales,removing last row (total) and changing column titles
  Data <- RawData[-nrow(RawData),]*1000
  names(Data) <- c(1984:2018)
  return(Data)
}

  # second transformation of the  dataset, common to all species
DataTrans2 <- function(ParLitt,Dat1){
  # Dat1 : a dataset once transformed, generated by DataTrans1_...
  # ParLitt is a set of realistic parameters for the species considered given by the litterature (all exept mu and om)
  #
  # it must give number by age, with first age class: a = a_rec
  #
  
  a_mat <- ParLitt$a_mat
  a_rec <- ParLitt$a_rec
  lag <- a_rec - a_mat
  
  Narec <- Dat1[1,] # number of individuals of age a = a_rec
  Nsup <- apply(Dat1[-1,], 2, sum) # number of individuals of age a > a_rec
  Data <- rbind(Narec,Nsup)
  
  row.names(Data)[1] <- 'Narec'
  row.names(Data)[2] <- 'Nsup'
  
  if(lag != 0){
    # in this case one needs data for Nk, k= amat,...,arec-1
    # but they are generally not available. Let us compute them as a function of Narec !
    S <- ParLitt$S
    
    Nk <- data.frame(matrix(nrow = lag, ncol = ncol(Dat1)))
    colnames(Nk) <- colnames(Dat1)
    for(k in 1:(lag)){
      for(i in seq(ncol(Dat1) - k)){
        Nk[lag-k+1,i] <- Narec[i+k] / ( S**(12*k) )
        row.names(Nk)[lag-k+1] <- paste0('Narec-',k)
      }
    }
    
    row.names(Nk)[1] <- 'Namat'
    Data <- rbind(Nk,Data)
  }
  
  return(Data)
}

DataTrans2.1.1 <- function(ParLitt,Dat1,Dmat_range){
  # a continuation of DataTrans 2 to consider a variety of values of parameter Dmat
  # calls params_DChange
  
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Data <- list()
  for(i in seq(length(Dmat_range))){
    par <- params_DChange(params = par, 
                          Dmat = Dmat_range[i], 
                          Drec = par$Drec)
    Dat_new <- DataTrans2(par, Dat1)
    Data[[i]] <- Dat_new
  }
  return(Data)
}
DataTrans2.1.2 <- function(ParLitt,Dat1,Drec_range){
  # a continuation of DataTrans 2 to consider a variety of values of parameter Drec
  # calls params_DChange
  
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Data <- list()
  for(i in seq(length(Drec_range))){
    par <- params_DChange(params = par, 
                          Dmat = par$Dmat, 
                          Drec = Drec_range[i])
    Dat_new <- DataTrans2(par, Dat1)
    Data[[i]] <- Dat_new
  }
  return(Data)
}
DataTrans2.1.3 <- function(ParLitt,Dat1,Dmat_range,Lag){
  # a continuation of DataTrans 2 to consider a variety of values of parameter Dmat and Drec when the lag is constant
  # calls params_DChange
  
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Data <- list()
  for(i in seq(length(Dmat_range))){
    par <- params_DChange(params = par, 
                          Dmat = Dmat_range[i], 
                          Drec = Dmat_range[i]+Lag)
    Dat_new <- DataTrans2(par, Dat1)
    Data[[i]] <- Dat_new
  }
  return(Data)
}
DataTrans2.1.4 <- function(ParLitt,Dat1,Dmat_range,Drec_range){
  # a continuation of DataTrans 2 to consider all combinations of values of parameter Dmat and Drec 
  # calls params_DChange
  Dmat <- Dmat_range
  Drec <- Drec_range
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Data <- list()
  Plan <- expand.grid(Dmat,Drec)
  colnames(Plan) <- c('Dmat','Drec')
  for(i in seq(nrow(Plan))){
    par <- params_DChange(params = par, 
                          Dmat = Plan$Dmat[i], 
                          Drec = Plan$Drec[i])
    Dat_new <- DataTrans2(par, Dat1)
    Data[[i]] <- Dat_new
  }
  return(Data)
}


  # transforming assessment data into a dataset suitable for estimating parameters
DataSR <- function(ParLitt,Dat2){
  # ParLitt is a set of realistic parameters for the species considered given by the litterature (all exept mu and om)
  # Dat2 : a dataset twice transformed, generated by DataTrans2
  
  a_mat <- ParLitt$a_mat
  a_rec <- ParLitt$a_rec
  lag <- a_rec - a_mat
  
  ## on which period can we predict the recruitment ?
  y_start <- a_rec+2
  y_stop <- ncol(Dat2)-lag
  
  ## building the dataset
  Narec <- t(Dat2['Narec',])
  
  Data <- Narec
  for(i in (lag+1):(a_rec+1) ){ 
    
    # applying the appropriate lag on data
    N <- t( Dat2[,1:(ncol(Dat2)-i)] )
    Fill <- matrix(nrow=i, ncol = ncol(N))
    N <- rbind(Fill,N)
    
    # naming the new group of variables
    Names <- c()
    for(n in seq(length(colnames(N)))){
      new_name <- paste0('Lag',i,'_',colnames(N)[n])
      Names <- c(Names, new_name)
    }
    colnames(N) <- Names
    row.names(N) <- c()
    
    # adding the new variables to the dataset
    Data <- cbind(Data,N)
  }
  
  Data <- Data[y_start:y_stop,]
  
  return(Data)
}

DataSR0.1 <- function(ParLitt,Dat2.1){
  # a continuation of DataSR made suitable to treat multiple datasets
  # for different Dmat/Drec (datasets generated by DataTrans2.1._)
  # calls DataSR
  
  Data <- list()
  for(i in seq(length(Dat2.1))){
    Dat_new <- DataSR(ParLitt = ParLitt, Dat2 = Dat2.1[[i]])
    Data[[i]] <- Dat_new
  }
  return(Data)
}

  # estimation of parameters strictly speaking
ParEstim <- function(ParLitt,DataSR){
  # ParLitt is a set of parameters generated by params..._litt
  # DataSR is a dataset of customed stock recruitment relationship generated by DataSR
  if(ParLitt$m_mat <= ParLitt$m_rec){
    Out <- ParEstim1(ParLitt,DataSR)
  }else{
    Out <- ParEstim2(ParLitt,DataSR)
  }
  return(Out)
}
ParEstim1 <- function(ParLitt,DataSR){
  # when m_mat <= m_rec
  # ParLitt is a set of parameters generated by params..._litt
  # DataSR is a dataset of customed stock recruitment relationship generated by DataSR

  
  # parameters of interest
  r <- ParLitt$r
  S <- ParLitt$S
  psi1 <- ParLitt$psi1
  psi2 <- ParLitt$psi2
  psi3 <- ParLitt$psi3
  
  beta3 <- ParLitt$beta3
  beta4 <- ParLitt$beta4
  theta3 <- ParLitt$theta3
  theta4 <- ParLitt$theta4
  kappa3 <- ParLitt$kappa3
  kappa4 <- ParLitt$kappa4

  a_mat <- ParLitt$a_mat
  a_rec <- ParLitt$a_rec
  lag <- a_rec-a_mat
  
  m_mat <- ParLitt$m_mat
  m_rec <- ParLitt$m_rec
  
  ## variable extraction
  
  n_class <-  2 + lag # the number of classes required for computing recruitment
  n_year <- (ncol(DataSR)-1)/n_class # the number of years for each class in the dataset
  
  # Psi : individuals of class N... participating to reproduction
  Nsup0 <- DataSR[,ncol(DataSR)] 
  Narec0 <- DataSR[,ncol(DataSR)-1]
  
  Nk0 <- 0
  if(lag !=0){
    for(k in 2:(n_class-1) ){
      Nk0 <- Nk0 + DataSR[,ncol(DataSR)-k]
    }
  }
  
  Psi <- psi1*Nk0 + psi2*Narec0 + psi3*Nsup0
  
  
  # phi : the function for density-dependance
  Nsup1 <- 0
  Narec1 <- 0
  Nk1 <- 0
  for(i in seq(n_year-1)){
    index <- 1 + i*n_class
    
    Nsup1 <- Nsup1 + DataSR[,index]  
    Narec1 <- Narec1 + DataSR[,index-1]
    
    if(lag !=0){
      for(k in 2:(n_class-1) ){
        Nk1 <- Nk1 + DataSR[,index - k]
      }
    }
  }
  
  phi <- beta3*Nk0 + theta3*Narec0 + kappa3*Nsup0 + beta4*Nk1 + theta4*Narec1 + kappa4*Nsup1
  
  ## expressing and fitting
  
  Narec <- DataSR[,1] # recruitment Data - to be predicted
  b <- m_mat + 12*a_mat 
  
  Data <- data.frame(Narec,Psi,phi)
  
  fit <- nls(formula = Narec ~ r * (S**(12*lag)) * Psi * exp(-mu*phi - b*om ),
             start = list(mu = 1e-10, om = 0.2),
             data = Data)

  return(fit)
}
ParEstim2 <- function(ParLitt,DataSR){
  # when m_mat > m_rec
  # ParLitt is a set of parameters generated by params..._litt
  # DataSR is a dataset of customed stock recruitment relationship generated by DataSR
  
  
  # parameters of interest
  r <- ParLitt$r
  S <- ParLitt$S
  Sh <- ParLitt$Sh
  
  chi <- ParLitt$chi
  sigma <- ParLitt$sigma
  
  psi1 <- ParLitt$psi1
  psi2 <- ParLitt$psi2
  psi3 <- ParLitt$psi3
  
  beta3 <- ParLitt$beta3
  beta4 <- ParLitt$beta4
  theta3 <- ParLitt$theta3
  theta4 <- ParLitt$theta4
  kappa3 <- ParLitt$kappa3
  kappa4 <- ParLitt$kappa4
  eta3 <- ParLitt$eta3
  eta4 <-ParLitt$eta4
  
  a_mat <- ParLitt$a_mat
  a_rec <- ParLitt$a_rec
  lag <- a_rec-a_mat
  
  m_mat <- ParLitt$m_mat
  m_rec <- ParLitt$m_rec
  
  ## variable extraction
  
  n_class <-  2 + lag # the number of classes required for computing recruitment
  n_year <- (ncol(DataSR)-1)/n_class # the number of years for each class in the dataset
  
  # Phi : individuals of class N... participating to reproduction
  Nsup0 <- DataSR[,ncol(DataSR)] 
  Narec0 <- DataSR[,ncol(DataSR)-1]
  Narec0inf <- DataSR[,ncol(DataSR)-2]
  
  Nk0 <- 0
  if(lag > 1){
    for(k in 2:(n_class-2) ){
      Nk0 <- Nk0 + DataSR[,ncol(DataSR)-k]
    }
  }
  
  Phi <- psi1*Nk0 + psi1*Narec0inf + psi3*Narec0 + psi3*Nsup0
  
  
  # Xi : the function for density-dependance
  Nsup1 <- 0
  Narec1 <- 0
  Narec1inf <- 0
  Nk1 <- 0
  for(i in seq(n_year-1)){
    index <- 1 + i*n_class
    
    Nsup1 <- Nsup1 + DataSR[,index]  
    Narec1 <- Narec1 + DataSR[,index-1]
    Narec1inf <- Narec1inf + DataSR[,index-2]
    
    if(lag >1){
      for(k in 2:(n_class-2) ){
        Nk1 <- Nk1 + DataSR[,index - k]
      }
    }
  }
  
  Xi <- beta3*Nk0 + eta3*Narec0inf + kappa3*Narec0 + kappa3*Nsup0 + beta4*Nk1 + eta4*Narec1inf + kappa4*Narec1 + kappa4*Nsup1
  
  ## expressing and fitting
  
  Narec <- DataSR[,1] # recruitment Data - to be predicted
  b <- m_mat + 12*a_mat 
  
  Data <- data.frame(Narec,Phi,Xi)
  
  fit <- nls(formula = Narec ~ r * chi * (sigma**(lag-1)) * Phi * exp(-mu*Xi - b*om ),
             start = list(mu = 1e-10, om = 0.2),
             data = Data)
  
  return(fit)
}


ParEstim0.1.1 <- function(ParLitt,DataSR,Dmat_range){
  # A continuation of ParEstim when DataSR is generated by DataSR0.1
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Fit <- list()
  for(i in seq(length(Dmat_range))){
    par <- params_DChange(params = par, 
                          Dmat = Dmat_range[i], 
                          Drec = par$Drec)
    Data <- DataSR[[i]]
    fit_new <- ParEstim(par,Data)
    Fit[[i]] <- fit_new
  }
  return(Fit)
}
ParEstim0.1.2 <- function(ParLitt,DataSR,Drec_range){
  # A continuation of ParEstim when DataSR is generated by DataSR0.1
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Fit <- list()
  for(i in seq(length(Drec_range))){
    par <- params_DChange(params = par, 
                          Dmat = par$Dmat, 
                          Drec = Drec_range[i])
    Data <- DataSR[[i]]
    fit_new <- ParEstim(par,Data)
    Fit[[i]] <- fit_new
  }
  return(Fit)
}
ParEstim0.1.3 <- function(ParLitt,DataSR,Dmat_range,Lag){
  # A continuation of ParEstim when DataSR is generated by DataSR0.1
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  Fit <- list()
  for(i in seq(length(Dmat_range))){
    par <- params_DChange(params = par, 
                          Dmat = Dmat_range[i], 
                          Drec = Dmat_range[i]+Lag)
    Data <- DataSR[[i]]
    fit_new <- ParEstim(par,Data)
    Fit[[i]] <- fit_new
  }
  return(Fit)
}
ParEstim0.1.4 <- function(ParLitt,DataSR,Dmat_range,Drec_range){
  # A continuation of ParEstim when DataSR is generated by DataSR0.1
  Dmat <- Dmat_range
  Drec <- Drec_range
  par <- ParLitt
  par$mu <- 0 # this parameter is unused at this stage but leaving it empty generates an error
  par$om <- 0 #idem
  
  Plan <- expand.grid(Dmat,Drec)
  colnames(Plan) <- c('Dmat','Drec')
  Fit <- list()
  for(i in seq(nrow(Plan))){
    par <- params_DChange(params = par, 
                          Dmat = Plan$Dmat[i], 
                          Drec = Plan$Drec[i])
    Data <- DataSR[[i]]
    fit_new <- ParEstim(par,Data)
    Fit[[i]] <- fit_new
  }
  return(Fit)
}


### modify a set of parameters ----

params_update01 <- function(params){
  # this function adresses the following problem : when a single raw parameter is modified in a function, 
  # the aggregated parameters that depend on this raw parameter must take this modification into account to avoid errors
  # one should run this function each time he modifies a value of a parameter in order to keep the parameter set coherent
  #
  # this particular function is designed to read a modified value of Sh ; 
  # another one will be designed to read values of Ma and Fa
  
  # raw parameters
  par_new <- list(
    r = params$r,
    S = params$S,
    Sh = params$Sh,
    mu = params$mu,
    Dmat = params$Dmat,
    Drec = params$Drec,
    om = params$om
  )
  
  # aggregated parameters - level 1
  par_new$a_mat <- par_new$Dmat %/% 12 # age of maturation
  par_new$m_mat <- par_new$Dmat %% 12 # month of maturation
  
  par_new$a_rec <- par_new$Drec %/% 12 # age of recruitment
  par_new$m_rec <- par_new$Drec %% 12 # month of recruitment
  
  
  # aggregated parameters - level 2
  
  par_new$alpha1 <- exp(- par_new$om * par_new$m_mat)
  par_new$alpha2 <- par_new$alpha1 * exp( -par_new$om * (12-par_new$m_mat) )
  
  b1 <- 0
  for(i in 0:(par_new$m_mat-1) ){ b1 <- b1 + par_new$S**(12 - par_new$m_mat + i) }
  par_new$beta1 <- par_new$mu * b1
  
  b2 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ b2 <- b2 + par_new$S**(i) }
  par_new$beta2 <- par_new$beta1 + par_new$mu * b2
  
  b3 <- 0
  for(i in 0:(par_new$m_mat-1) ){ b3 <- b3 + par_new$S**(12 - par_new$m_mat + i) }
  par_new$beta3 <- b3
  
  b4 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ b4 <- b4 + par_new$S**(i) }
  par_new$beta4 <- par_new$beta3 + b4
  
  
  t1 <- 0
  for(i in 0:(par_new$m_mat-1) ){ t1 <- t1 + par_new$Sh**(12 - par_new$m_rec + i) }
  par_new$theta1 <- par_new$mu * (par_new$S**(par_new$m_rec - par_new$m_mat) ) * t1
  
  t21 <- 0
  t22 <- 0
  if(par_new$m_mat!=par_new$m_rec){
    for(i in 0:(par_new$m_rec - par_new$m_mat -1) ){ t21 <- t21 + par_new$S**(i) }
  }
  for(i in 0:(12 - par_new$m_rec - 1)){ t22 <- t22 + par_new$Sh**(i) }
  par_new$theta2 <- par_new$theta1 + par_new$mu * ( t21 + par_new$S**(par_new$m_rec - par_new$m_mat)*t22 )
  
  t3 <- 0
  for(i in 0:(par_new$m_mat-1) ){ t3 <- t3 + par_new$Sh**(12 - par_new$m_rec + i) }
  par_new$theta3 <- (par_new$S**(par_new$m_rec - par_new$m_mat) ) * t3  
  
  t41 <- 0
  t42 <- 0
  for(i in 0:(par_new$m_rec - par_new$m_mat -1) ){ t41 <- t41 + par_new$S**(i) }
  for(i in 0:(12 - par_new$m_rec - 1)){ t42 <- t42 + par_new$Sh**(i) }
  par_new$theta4 <- par_new$theta3 + ( t41 + par_new$S**(par_new$m_rec - par_new$m_mat)*t42 )
  
  k1 <- 0
  for(i in 0:(par_new$m_mat-1) ){ k1 <- k1 + par_new$Sh**(12 - par_new$m_mat + i) }
  par_new$kappa1 <- par_new$mu * k1
  
  k2 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ k2 <- k2 + par_new$Sh**(i) }
  par_new$kappa2 <- par_new$kappa1 + par_new$mu * k2
  
  k3 <- 0
  for(i in 0:(par_new$m_mat-1) ){ k3 <- k3 + par_new$Sh**(12 - par_new$m_mat + i) }
  par_new$kappa3 <- k3
  
  k4 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ k4 <- k4 + par_new$Sh**(i) }
  par_new$kappa4 <- par_new$kappa3 + k4
  
  
  e11 <- 0
  e12 <- 0
  for(i in 0:(par_new$m_mat - par_new$m_rec -1)){e11 <- e11 + par_new$Sh**i}
  for(i in 0:(par_new$m_rec -1)){e12 <- e12 + par_new$S**(12-par_new$m_mat+i) }
  par_new$eta1 <- par_new$mu * ( (par_new$S**(12+ par_new$m_rec -par_new$m_mat)*e11) + e12 )
  
  e2 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){e2 <- e2 + par_new$S**i}
  par_new$eta2  <- par_new$eta1 + par_new$mu * e2
  
  e31 <- 0
  e32 <- 0
  for(i in 0:(par_new$m_mat - par_new$m_rec -1)){e31 <- e31 + par_new$Sh**i}
  for(i in 0:(par_new$m_rec -1)){e32 <- e32 + par_new$S**(12-par_new$m_mat+i) }
  par_new$eta3 <- ( (par_new$S**(12+ par_new$m_rec -par_new$m_mat)*e31) + e32 )
  
  e4 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){e4 <- e4 + par_new$S**i}
  par_new$eta4  <- par_new$eta3 + e4
  
 
  
  # aggregated parameters - level 3
  
  par_new$sigma <- par_new$S**12
  par_new$nu <- par_new$Sh**12
  par_new$rho <- (par_new$S**(par_new$m_rec-par_new$m_mat)) * (par_new$Sh**(12-(par_new$m_rec-par_new$m_mat)))
  par_new$chi <- (par_new$S**(12-(par_new$m_mat-par_new$m_rec))) * (par_new$Sh**(par_new$m_mat-par_new$m_rec))
  
  
  par_new$alpha <- par_new$r * par_new$alpha1 * (par_new$alpha2 ** (par_new$a_mat))
  par_new$beta <- par_new$beta1 + par_new$a_mat * par_new$beta2
  par_new$theta <- par_new$theta1 + par_new$a_mat * par_new$theta2
  par_new$kappa <- par_new$kappa1 + par_new$a_mat * par_new$kappa2
  par_new$eta <- par_new$eta1 + par_new$a_mat * par_new$eta2
  
  
  par_new$psi1 <- par_new$S**(12-par_new$m_mat)
  par_new$psi2 <- (par_new$S**(par_new$m_rec-par_new$m_mat)) * (par_new$Sh**(12-par_new$m_rec))
  par_new$psi3 <- par_new$Sh**(12-par_new$m_mat)
  
  
  
  return(par_new)
  
}

params_update02 <- function(params){
  # this function adresses the following problem : when a single raw parameter is modified in a function, 
  # the aggregated parameters that depend on this raw parameter must take this modification into account to avoid errors
  # one should run this function each time he modifies a value of a parameter in order to keep the parameter set coherent
  #
  # this particular function is designed to read a modified values of Ma and Fa ; 
  # another one will be designed to read values of Sh
  
  # raw parameters
  par_new <- list(
    r = params$r,
    Ma = params$Ma,
    Fa = params$Fa,
    mu = params$mu,
    Dmat = params$Dmat,
    Drec = params$Drec,
    om = params$om
  )
  
  # aggregated parameters - level 1
  par_new$a_mat <- par_new$Dmat %/% 12 # age of maturation
  par_new$m_mat <- par_new$Dmat %% 12 # month of maturation
  
  par_new$a_rec <- par_new$Drec %/% 12 # age of recruitment
  par_new$m_rec <- par_new$Drec %% 12 # month of recruitment
  
  par_new$S <- exp(- par_new$Ma) #survival rate of unfished individuals of age a
  par_new$Sh <- exp(- par_new$Ma - par_new$Fa) # survival rate of harvested individuals of age >=a
  
  par_new$beta <- exp(- 12 * par_new$om)
  
  # aggregated parameters - level 2
  
  par_new$alpha1 <- exp(- par_new$om * par_new$m_mat)
  par_new$alpha2 <- par_new$alpha1 * exp( -par_new$om * (12-par_new$m_mat) )
  
  b1 <- 0
  for(i in 0:(par_new$m_mat-1) ){ b1 <- b1 + par_new$S**(12 - par_new$m_mat + i) }
  par_new$beta1 <- par_new$mu * b1
  
  b2 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ b2 <- b2 + par_new$S**(i) }
  par_new$beta2 <- par_new$beta1 + par_new$mu * b2
  
  b3 <- 0
  for(i in 0:(par_new$m_mat-1) ){ b3 <- b3 + par_new$S**(12 - par_new$m_mat + i) }
  par_new$beta3 <- b3
  
  b4 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ b4 <- b4 + par_new$S**(i) }
  par_new$beta4 <- par_new$beta3 + b4
  
  t1 <- 0
  for(i in 0:(par_new$m_mat-1) ){ t1 <- t1 + par_new$Sh**(12 - par_new$m_rec + i) }
  par_new$theta1 <- par_new$mu * (par_new$S**(par_new$m_rec - par_new$m_mat) ) * t1
  
  t21 <- 0
  t22 <- 0
  if(par_new$m_mat!=par_new$m_rec){
    for(i in 0:(par_new$m_rec - par_new$m_mat -1) ){ t21 <- t21 + par_new$S**(i) }
  }
  for(i in 0:(12 - par_new$m_rec - 1)){ t22 <- t22 + par_new$Sh**(i) }
  par_new$theta2 <- par_new$theta1 + par_new$mu * ( t21 + par_new$S**(par_new$m_rec - par_new$m_mat)*t22 )
  
  t3 <- 0
  for(i in 0:(par_new$m_mat-1) ){ t3 <- t3 + par_new$Sh**(12 - par_new$m_rec + i) }
  par_new$theta3 <- (par_new$S**(par_new$m_rec - par_new$m_mat) ) * t3  
  
  t41 <- 0
  t42 <- 0
  for(i in 0:(par_new$m_rec - par_new$m_mat -1) ){ t41 <- t41 + par_new$S**(i) }
  for(i in 0:(12 - par_new$m_rec - 1)){ t42 <- t42 + par_new$Sh**(i) }
  par_new$theta4 <- par_new$theta3 + ( t41 + par_new$S**(par_new$m_rec - par_new$m_mat)*t42 )
  
  k1 <- 0
  for(i in 0:(par_new$m_mat-1) ){ k1 <- k1 + par_new$Sh**(12 - par_new$m_mat + i) }
  par_new$kappa1 <- par_new$mu * k1
  
  k2 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ k2 <- k2 + par_new$Sh**(i) }
  par_new$kappa2 <- par_new$kappa1 + par_new$mu * k2
  
  k3 <- 0
  for(i in 0:(par_new$m_mat-1) ){ k3 <- k3 + par_new$Sh**(12 - par_new$m_mat + i) }
  par_new$kappa3 <- k3
  
  k4 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){ k4 <- k4 + par_new$Sh**(i) }
  par_new$kappa4 <- par_new$kappa3 + k4
  
  
  e11 <- 0
  e12 <- 0
  for(i in 0:(par_new$m_mat - par_new$m_rec -1)){e11 <- e11 + par_new$Sh**i}
  for(i in 0:(par_new$m_rec -1)){e12 <- e12 + par_new$S**(12-par_new$m_mat+i) }
  par_new$eta1 <- par_new$mu * ( (par_new$S**(12+ par_new$m_rec -par_new$m_mat)*e11) + e12 )
  
  e2 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){e2 <- e2 + par_new$S**i}
  par_new$eta2  <- par_new$eta1 + par_new$mu * e2
  
  e31 <- 0
  e32 <- 0
  for(i in 0:(par_new$m_mat - par_new$m_rec -1)){e31 <- e31 + par_new$Sh**i}
  for(i in 0:(par_new$m_rec -1)){e32 <- e32 + par_new$S**(12-par_new$m_mat+i) }
  par_new$eta3 <- ( (par_new$S**(12+ par_new$m_rec -par_new$m_mat)*e31) + e32 )
  
  e4 <- 0
  for(i in 0:(12-par_new$m_mat-1) ){e4 <- e4 + par_new$S**i}
  par_new$eta4  <- par_new$eta3 + e4
  
  
  # aggregated parameters - level 3
  
  par_new$sigma <- par_new$S**12
  par_new$nu <- par_new$Sh**12
  par_new$rho <- (par_new$S**(par_new$m_rec-par_new$m_mat)) * (par_new$Sh**(12-(par_new$m_rec-par_new$m_mat)))
  par_new$chi <- (par_new$S**(12-(par_new$m_mat-par_new$m_rec))) * (par_new$Sh**(par_new$m_mat-par_new$m_rec))
  
  
  par_new$alpha <- par_new$r * par_new$alpha1 * (par_new$alpha2 ** (par_new$a_mat))
  par_new$beta <- par_new$beta1 + par_new$a_mat * par_new$beta2
  par_new$theta <- par_new$theta1 + par_new$a_mat * par_new$theta2
  par_new$kappa <- par_new$kappa1 + par_new$a_mat * par_new$kappa2
  par_new$eta <- par_new$eta1 + par_new$a_mat * par_new$eta2
  
  
  par_new$psi1 <- par_new$S**(12-par_new$m_mat)
  par_new$psi2 <- (par_new$S**(par_new$m_rec-par_new$m_mat)) * (par_new$Sh**(12-par_new$m_rec))
  par_new$psi3 <- par_new$Sh**(12-par_new$m_mat)
  
  
  return(par_new)
  
}

params_DChange <- function(params, Dmat, Drec){
  # reads a predefined paramater set, change values of Dmat and Drec, and makes the necessary updates
  # calls params_update02
  par_new <- params
  par_new$Dmat <- Dmat
  par_new$Drec <- Drec
  par_new <- params_update02(par_new)
  return(par_new)
}

params_rFaChange <- function(params, r, Fa){
  # reads a predefined paramater set, change values of r and Fa, and makes the necessary updates
  # calls params_update02
  par_new <- params
  par_new$r <- r
  par_new$Fa <- Fa
  par_new <- params_update02(par_new)
  return(par_new)
}



### ranges of parameters ----
Fa_range <- function(Fa_min, Fa_max){ 
  Fa <- seq(Fa_min, Fa_max, length=1000)
  return(Fa) 
}

r_range <- function(r_min, r_max){ 
  r <- seq(r_min, r_max, length=1000)
  return(r) 
}

r_range2 <- function(r_min, r_max){ 
  r <- seq(r_min, r_max, length=7)
  return(r) 
}

r_range3 <- function(r_min, r_max){ 
  r <- seq(r_min, r_max, length=100)
  return(r) 
}

mu_range <- function(mu_min, mu_max){
  mu <- seq(mu_min, mu_max, length=1000)
  return(mu)
}

Drange <- function(Dmin, Dmax, step=1){
  # return a range of values of Dmat or Drec
  # D takes only integer values
  D <- seq(Dmin, Dmax, step)
  return(D)
}




### describe variations of estimations of parameters mu and om----
VarEstim <- function(ParData){
  # ParData generated by ParFill0.1, an ensemble of parameter sets
  if(ParData[[1]]$Dmat != ParData[[2]]$Dmat){#testing if we must consider a Dmat or Drec variation
    
    Dmat <- c()
    mu <- c()
    om <- c()
    for(i in seq(length(ParData))){
      Dmat <- c(Dmat,ParData[[i]]$Dmat)
      mu <- c(mu,ParData[[i]]$mu)
      om <- c(om,ParData[[i]]$om)
    }
    
    Out <- data.frame(Dmat,mu,om)
    
  }else{
    
    Drec <- c()
    mu <- c()
    om <- c()
    for(i in seq(length(ParData))){
      Drec <- c(Drec,ParData[[i]]$Drec)
      mu <- c(mu,ParData[[i]]$mu)
      om <- c(om,ParData[[i]]$om)
    }
    
    Out <- data.frame(Drec,mu,om)
    
    
  }
  
  return(Out)
}

VarEstim2.1 <- function(ParData){
  # returns  one matrix for mucontaining estimations of this parameters
  # when crossing a value of Dmat and a value of Drec
  # ParData generated by ParFill0.1.4, an ensemble of parameter sets
  
  
    
    Dmat <- c()
    Drec <- c()
    mu <- c()
    om <- c()
    for(i in seq(length(ParData))){
      Dmat <- c(Dmat,ParData[[i]]$Dmat)
      Drec <- c(Drec,ParData[[i]]$Drec)
      mu <- c(mu,ParData[[i]]$mu)
    }
    
    Dat1 <- data.frame(Dmat,Drec,mu)
    
    Dmat_range <- unique(Dat1$Dmat)
    Drec_range <- unique(Dat1$Drec)
    Out1 <- data.frame()
    for(j in seq(length(Dmat_range))){
      new_line1 <- Dat1[Dat1$Dmat==Dmat_range[j],'mu']
      Out1 <- rbind(Out1,new_line1)
    }
  colnames(Out1) <- as.character(Drec_range)
  rownames(Out1) <- as.character(Dmat_range)
  
  return(Out1)
}
VarEstim2.2 <- function(ParData){
  # returns  one matrix for om containing estimations of this parameter
  # when crossing a value of Dmat and a value of Drec
  # ParData generated by ParFill0.1.4, an ensemble of parameter sets
  
  
  
  Dmat <- c()
  Drec <- c()
  mu <- c()
  om <- c()
  for(i in seq(length(ParData))){
    Dmat <- c(Dmat,ParData[[i]]$Dmat)
    Drec <- c(Drec,ParData[[i]]$Drec)
    om <- c(om,ParData[[i]]$om)
  }
  
  Dat2 <- data.frame(Dmat,Drec,om)
  
  Dmat_range <- unique(Dat2$Dmat)
  Drec_range <- unique(Dat2$Drec)
  Out2 <- data.frame()
  for(j in seq(length(Dmat_range))){
    new_line2 <- Dat2[Dat2$Dmat==Dmat_range[j],'om']                 
    Out2 <- rbind(Out2,new_line2)
  }
  colnames(Out2) <- as.character(Drec_range)
  rownames(Out2) <- as.character(Dmat_range)
  
  return(Out2)
}

