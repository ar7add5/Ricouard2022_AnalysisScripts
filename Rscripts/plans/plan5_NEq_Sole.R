##############################################################################
##############################################################################
# 
# Project : Ricouard, Lehuta, Mahévas (2022) - Are Maximum Yields sustainable ?
# 
# Script : plan5_NEq_Sole
# 
# Obj : Define workflow for the analysis of the equilibrium
#       when the model is parameterized for sole (presented in appendix)
#       
# Contact : antoine.ricouard@ifremer.fr
# 
##############################################################################
##############################################################################

plan5 <- drake_plan(
  
  ### parameters settings----
  
  # parameters specified from litterature review
  par0_sole = paramsSole01_litt(),
  
  # data processing
  Dat0_sole = DataRead_txt('Solea_solea_assessment.txt'),
  Dat1_sole = DataTrans1_sole(Dat0_sole),
  Dat2_sole = DataTrans2(ParLitt = par0_sole, 
                              Dat1 = Dat1_sole),
  SRdata_sole = DataSR(ParLitt = par0_sole, 
                            Dat2 = Dat2_sole),
  
  # complete the parameter set with missing values
  Fit_sole = ParEstim(ParLitt = par0_sole, 
                           DataSR = SRdata_sole),
  par1_sole = ParFill(ParLitt = par0_sole, 
                           Fit = Fit_sole),
  
 
  # ranges of parameters
  RecRange1 = Drange(Dmin = 37, Dmax = 47),
  
  MatRange1= Drange(Dmin = 25, Dmax = 30),
  
  FaRange2 = Fa_range(Fa_min = 0, Fa_max = 0.03),
  FaRange5 = Fa_range(Fa_min = 0, Fa_max = 0.1),
 
  ### compute Namat* as a function of Fa for different values of Dmat and Drec----
  Namat1_FaDmat_sole = NamatEq_Data_FaDmat(params = par1_sole, 
                                                Fa_range = FaRange5, 
                                                Dmat_range = MatRange1),
  plotNamat1_FaDmat_sole = Namat_plot_FaDmat(equilibrium_data = Namat1_FaDmat_sole),
  
  Namat1_FaDrec_sole = NamatEq_Data_FaDrec(params = par1_sole, 
                                                Fa_range = FaRange2, 
                                                Drec_range = RecRange1),
  plotNamat1_FaDrec_sole = Namat_plot_FaDrec(equilibrium_data = Namat1_FaDrec_sole),
  
  
  Namat1_FaCstLag_sole = NamatEq_Data_FaCstLag(params = par1_sole, 
                                                    Fa_range = FaRange5, 
                                                    Dmat_range = MatRange1, 
                                                    Lag = 14),
  plotNamat1_FaCstLag_sole = Namat_plot_FaDmat(equilibrium_data = Namat1_FaCstLag_sole),
  
  
  ### compute sum of reproducers at equilibrium as a function of Fa for different values of Dmat and Drec----
  SGen1_FaDmat_sole = SGenEq_Data_FaDmat(params = par1_sole, 
                                             Fa_range = FaRange5, 
                                             Dmat_range = MatRange1),
  plotSGen1_FaDmat_sole = SGenEq_plot_FaDmat(equilibrium_data = SGen1_FaDmat_sole),
  
  SGen1_FaDrec_sole = SGenEq_Data_FaDrec(params = par1_sole, 
                                             Fa_range = FaRange2, 
                                             Drec_range = RecRange1),
  plotSGen1_FaDrec_sole = SGenEq_plot_FaDrec(equilibrium_data = SGen1_FaDrec_sole),
  
  
  SGen1_FaCstLag_sole = SGenEq_Data_FaCstLag(params = par1_sole, 
                                                 Fa_range = FaRange5, 
                                                 Dmat_range = MatRange1, 
                                                 Lag = 2),
  plotSGen1_FaCstLag_sole = SGenEq_plot_FaDmat(equilibrium_data = SGen1_FaCstLag_sole),
  
  
  
)
