##############################################################################
##############################################################################
# 
# Project : Ricouard, Lehuta, Mahévas (2022) - Are Maximum Yields sustainable ?
# 
# Script : plan0.1_Estimation_Sole
# 
# Obj : Define workflow for the analysis in which estimation of mu and omega are estimed at each modification of Dmat/Drec. 
#       Most of this plan is repeted in subsequent plans
#       
# Contact : antoine.ricouard@ifremer.fr
# 
##############################################################################
##############################################################################

plan0.1 <- drake_plan(
  
  ### parameters settings----
  
  # parameters specified from litterature review
  par0_sole = paramsSole01_litt(),
  
  # ranges of parameters
  RecRange1 = Drange(Dmin = 37, Dmax = 46),
  MatRange1= Drange(Dmin = 25, Dmax = 30),
  
  
  # data processing
  Dat0_sole = DataRead_txt('Solea_solea_assessment.txt'),
  Dat1_sole = DataTrans1_sole(Dat0_sole),
  
  Dat2.1.1_sole = DataTrans2.1.1(ParLitt = par0_sole,
                                 Dat1 = Dat1_sole,
                                 Dmat_range = MatRange1),
  Dat2.1.2_sole = DataTrans2.1.2(ParLitt = par0_sole,
                                 Dat1 = Dat1_sole,
                                 Drec_range = RecRange1),
  Dat2.1.3_sole = DataTrans2.1.3(ParLitt = par0_sole,
                                 Dat1 = Dat1_sole,
                                 Dmat_range = MatRange1, 
                                 Lag = 14),
  Dat2.1.4_sole = DataTrans2.1.4(ParLitt = par0_sole,
                                 Dat1 = Dat1_sole,
                                 Dmat_range = MatRange1, 
                                 Drec_range = RecRange1),
  
  
  SRdata0.1.1_sole = DataSR0.1(ParLitt = par0_sole, 
                               Dat2 = Dat2.1.1_sole),
  SRdata0.1.2_sole = DataSR0.1(ParLitt = par0_sole, 
                               Dat2 = Dat2.1.2_sole),
  SRdata0.1.3_sole = DataSR0.1(ParLitt = par0_sole, 
                               Dat2 = Dat2.1.3_sole),
  SRdata0.1.4_sole = DataSR0.1(ParLitt = par0_sole, 
                               Dat2 = Dat2.1.4_sole),
  
  # estimating parameters
  Fit0.1.1_sole = ParEstim0.1.1(ParLitt = par0_sole,
                                DataSR = SRdata0.1.1_sole,
                                Dmat_range = MatRange1),
  Fit0.1.2_sole = ParEstim0.1.2(ParLitt = par0_sole,
                                DataSR = SRdata0.1.2_sole,
                                Drec_range = RecRange1),
  Fit0.1.3_sole = ParEstim0.1.3(ParLitt = par0_sole,
                                DataSR = SRdata0.1.3_sole,
                                Dmat_range = MatRange1,
                                Lag = 14),
  Fit0.1.4_sole = ParEstim0.1.4(ParLitt = par0_sole,
                                DataSR = SRdata0.1.4_sole,
                                Dmat_range = MatRange1,
                                Drec_range = RecRange1),
  
  
  par1.1.1_sole = ParFill0.1.1(ParLitt = par0_sole,
                               Fit = Fit0.1.1_sole,
                               Dmat_range = MatRange1),
  par1.1.2_sole = ParFill0.1.2(ParLitt = par0_sole, 
                               Fit = Fit0.1.2_sole,
                               Drec_range = RecRange1),
  par1.1.3_sole = ParFill0.1.3(ParLitt = par0_sole,
                               Fit = Fit0.1.3_sole,
                               Dmat_range = MatRange1,
                               Lag = 14),
  par1.1.4_sole = ParFill0.1.4(ParLitt = par0_sole,
                               Fit = Fit0.1.4_sole,
                               Dmat_range = MatRange1,
                               Drec_range = RecRange1),
  
  
  # assessing estimation
  EstimData1.1 = VarEstim(par1.1.1_sole),
  EstimData1.2 = VarEstim(par1.1.2_sole),
  EstimData1.3 = VarEstim(par1.1.3_sole),
  EstimData1.4.1 = VarEstim2.1(par1.1.4_sole),# matrix for mu
  EstimData1.4.2 = VarEstim2.2(par1.1.4_sole)# matrix for om
  
)